# Haught Codeworks Code Challenge

## Introduction
This library parses a provided CSV file (`bus_route_initial`) into the following models, stored in a PostgreSQL database:

- **Rider** - A rider on a bus, with a `first_name` and `last_name`
- **BusRoute** - A route that a bus travels, has a `name`
- **BusStop** - A stop on a bus route, with an `address` and `time`

## Setup

Prerequisites:

* Ruby (Tested on 2.5.1)
* Bundler
* PostgreSQL (Tested on 10.5)

At the root level of the project, run:

`bundle install`

to install the required gems.

Next run:

`rake db:create` and `rake db:migrate`

to create and structure the required databases.

Finally, run:

`rake haught:parse_csv`

to load the data within `bus_route_initial.csv`.

## Testing and Coverage

All testing and coverage is handled via `RSpec` and `SimpleCov`. The tests can be ran with:

`rspec`

at the root level, and the coverage report can be opened with:

`open coverage/index.html`