require 'csv'
require 'support/models'

class CSVImporter
    attr_reader :data

    def initialize(filename)
        @data = load(filename)
    end
    
    def delete_all
        RidersStop.delete_all
        RidersRoute.delete_all
        Rider.delete_all
    end

    def load(filename)
        return nil if filename == ''

        data = { routes: {}, riders: [] }

        CSV.foreach(filename, headers: true, header_converters: :symbol) do |row|
            data[:routes] = parse_route(data[:routes], row[:pickup_route], row[:pickup_bus_stop], row[:pickup_time])
            data[:routes] = parse_route(data[:routes], row[:dropoff_route], row[:dropoff_bus_stop], row[:dropoff_time])

            data[:riders] << parse_rider(row)
        end

        data
    end

    def import_routes_and_stops
        @data[:routes].each do |route, stops| 
            route = Route.find_or_create_by(name: route)
            
            stops.each do |address, times|
                times.each do |time|
                    route.stops.find_or_create_by(time: time, address: address)
                end
            end
        end
    end

    def import_riders
        @data[:riders].each do |rider|
            new_rider = Rider.create(first_name: rider[:first_name], last_name: rider[:last_name])
            
            new_rider.routes << Route.find_or_create_by(name: rider[:pickup_route])
            new_rider.routes << Route.find_or_create_by(name: rider[:dropoff_route])

            new_rider.stops << Stop.find_or_create_by(time: rider[:pickup_time], address: rider[:pickup_stop] )
            new_rider.stops << Stop.find_or_create_by(time: rider[:dropoff_time], address: rider[:dropoff_stop] )
        end
    end

    def record_count_print
        puts "found #{Rider.count} Riders, #{Stop.count} Stops, and #{Route.count} Routes"
        puts "found #{RidersStop.count} RidersStops, #{RidersRoute.count} RidersRoutes, and #{RoutesStop.count} RoutesStops"
    end

    private

    def parse_route(data, route, stop, time)
        data[route] = {} if data[route].nil?
        new_route = data[route]

        new_route[stop] = [] if new_route[stop].nil?
        new_stop = new_route[stop]

        new_stop << time unless new_stop.include?(time)
        
        data
    end

    def parse_rider(row)
        {
            first_name: row[:first_name],
            last_name: row[:last_name],
            pickup_route: row[:pickup_route],
            dropoff_route: row[:dropoff_route],
            pickup_stop: row[:pickup_bus_stop],
            dropoff_stop: row[:dropoff_bus_stop],
            pickup_time: row[:pickup_time],
            dropoff_time: row[:dropoff_time]
        }
    end
end