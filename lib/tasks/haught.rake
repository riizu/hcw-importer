require 'support/ar_helper'
require 'importers/csv_importer'

namespace :haught do
    task :parse_csv do
        puts 'Beginning CSV parsing process!'

        puts 'Connecting to DB...'
        AR.connect('development')

        csv_importer = CSVImporter.new('bus_route_initial.csv')

        csv_importer.record_count_print

        puts "Deleting Rider data and associations now"
        csv_importer.delete_all

        csv_importer.record_count_print
        
        puts "Importing routes and stops"
        csv_importer.import_routes_and_stops

        puts "Importing riders"
        csv_importer.import_riders
        
        csv_importer.record_count_print

        puts 'CSV parse complete!'
    end
end