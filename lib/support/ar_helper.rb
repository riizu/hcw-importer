module AR
    require 'active_record'

    def self.db_configuration
        db_configuration_file = File.join(File.expand_path('..', __FILE__), '..', '..', 'db', 'config.yml')
        YAML.load(File.read(db_configuration_file))
    end

    def self.connect(env)
        ActiveRecord::Base.establish_connection(db_configuration[env])
    end
end