require_relative 'rider'
require_relative 'route'

class RidersRoute < ActiveRecord::Base
    validates :rider_id, presence: true
    validates :route_id, presence: true

    belongs_to :rider
    belongs_to :route
end