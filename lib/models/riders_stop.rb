require_relative 'rider'
require_relative 'stop'

class RidersStop < ActiveRecord::Base
    validates :rider_id, presence: true
    validates :stop_id, presence: true

    belongs_to :rider
    belongs_to :stop
end