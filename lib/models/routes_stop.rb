require_relative 'route'
require_relative 'stop'

class RoutesStop < ActiveRecord::Base
    validates :route_id, presence: true
    validates :stop_id, presence: true

    belongs_to :route
    belongs_to :stop
end