require_relative 'riders_stop'
require_relative 'routes_stop'
require_relative 'rider'
require_relative 'route'

class Stop < ActiveRecord::Base
    validates :time, presence: true
    validates :address, presence: true

    has_many :riders_stops
    has_many :routes_stops
    has_many :riders, through: :riders_stops
    has_many :routes, through: :routes_stops
end