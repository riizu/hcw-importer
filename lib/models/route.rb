require_relative 'riders_route'
require_relative 'routes_stop'
require_relative 'rider'
require_relative 'stop'

class Route < ActiveRecord::Base
    validates :name, presence: true, uniqueness: true

    has_many :riders_routes
    has_many :routes_stops
    has_many :riders, through: :riders_routes
    has_many :stops, through: :routes_stops
end