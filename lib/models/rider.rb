require_relative 'riders_route'
require_relative 'riders_stop'
require_relative 'route'
require_relative 'stop'

class Rider < ActiveRecord::Base
    validates :first_name, presence: true
    validates :last_name, presence: true

    has_many :riders_routes
    has_many :riders_stops
    has_many :routes, through: :riders_routes
    has_many :stops, through: :riders_stops
end