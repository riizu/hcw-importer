require 'spec_helper'
require 'importers/csv_importer'

RSpec.describe CSVImporter, type: :model do
    describe '#delete_all' do
        subject { CSVImporter.new('') }

        context 'with riders' do
            before { 2.times { Rider.create(first_name: 'Jon', last_name: 'Doe') } }

            it 'removes all riders' do
                subject.delete_all
                expect(Rider.all.count).to eq 0
            end

            context 'with routes' do
                before do
                    rider = Rider.first
                    rider.routes.create(name: 'L08 MID1')
                    rider.routes.create(name: 'L08 MID2')
                end

                it 'removes all rider routes' do
                    subject.delete_all
                    expect(RidersRoute.all.count).to eq 0
                end

                it 'keeps all individual routes' do
                    subject.delete_all
                    expect(Route.all.count).to eq 2
                end
            end

            context 'with stops' do
                before do
                    rider = Rider.first
                    rider.stops.create(time: Time.now(), address: '1234 Fake Street')
                    rider.stops.create(time: Time.now() + 1.hour, address: '1234 Fake Street')
                end

                it 'removes all rider stops' do
                    subject.delete_all
                    expect(RidersStop.all.count).to eq 0
                end

                it 'keeps all individual stops' do
                    subject.delete_all
                    expect(Stop.all.count).to eq 2
                end
            end
        end
    end

    describe '#load' do
        context 'when there is not file' do
            subject { CSVImporter.new('') }
            it { expect(subject.data).to eq nil  }
        end

        context 'when there is a file' do
            Dir.mkdir('tmp') unless Dir.exist?('tmp')
            
            let(:file_path) { 'tmp/test.csv' }
            
            let!(:csv) do
                CSV.open(file_path, "w") do |csv|
                    rows.each do |row|
                        csv << row.split(",")
                    end
                end
            end

            let(:header) { 'Last Name,First Name,Pickup Route,Pickup Time,Pickup Bus Stop,Dropoff Route,Dropoff Time,Dropoff Bus Stop' }
            let(:row2) { 'Bailey,Barry,L08 MID2,12:26 PM,3631 E Mineral Pl,L58 PM 4,3:45 PM,3631 E Mineral Pl' }
            let(:rows) { [header, row2] }

            subject { CSVImporter.new(file_path) }

            after(:each) { File.delete(file_path) }

            context 'when there is one rider' do
                it "has a 'data' object" do
                    expect(subject.data).to_not eq nil
                end
    
                it "loads the 'data' object with a 'riders' and 'routes' object" do
                    expect(subject.data[:riders]).to_not eq nil
                    expect(subject.data[:routes]).to_not eq nil
                end
    
                it 'appropriately nests routes and stops' do
                    expected_routes_object = {
                        'L08 MID2' => { '3631 E Mineral Pl' => ['12:26 PM'] },
                        'L58 PM 4' => { '3631 E Mineral Pl' => ['3:45 PM'] }
                    }
    
                    expect(subject.data[:routes]).to eq expected_routes_object
                end
    
                it 'appropriately organizes riders' do
                    expected_riders_object = [
                        {
                            first_name: 'Barry',
                            last_name: 'Bailey',
                            pickup_route: 'L08 MID2',
                            dropoff_route: 'L58 PM 4',
                            pickup_stop: '3631 E Mineral Pl',
                            dropoff_stop: '3631 E Mineral Pl',
                            pickup_time: '12:26 PM',
                            dropoff_time: '3:45 PM'
                        }
                    ]
    
                    expect(subject.data[:riders]).to eq expected_riders_object
                end
            end

            context 'when there is more than one rider' do
                let(:row3) { 'Berry,Conlan,L58/L14,12:29 PM,6322 S LAKEVIEW ST,L53 PM 4,4:00 PM,6322 S LAKEVIEW ST' }
                let(:rows) { [header, row2, row3] }

                it 'appropriately nests multiple routes and stops' do
                    expected_routes_object = {
                        'L08 MID2' => { '3631 E Mineral Pl' => ['12:26 PM'] },
                        'L58 PM 4' => { '3631 E Mineral Pl' => ['3:45 PM'] },
                        'L58/L14' => { '6322 S LAKEVIEW ST' => ['12:29 PM'] },
                        'L53 PM 4' => { '6322 S LAKEVIEW ST' => ['4:00 PM'] }
                    }

                    expect(subject.data[:routes]).to eq expected_routes_object
                end

                it 'appropriately organizes multiple riders' do
                    expected_riders_object = [
                        {
                            first_name: 'Barry',
                            last_name: 'Bailey',
                            pickup_route: 'L08 MID2',
                            dropoff_route: 'L58 PM 4',
                            pickup_stop: '3631 E Mineral Pl',
                            dropoff_stop: '3631 E Mineral Pl',
                            pickup_time: '12:26 PM',
                            dropoff_time: '3:45 PM'
                        },
                        {
                            first_name: 'Conlan',
                            last_name: 'Berry',
                            pickup_route: 'L58/L14',
                            dropoff_route: 'L53 PM 4',
                            pickup_stop: '6322 S LAKEVIEW ST',
                            dropoff_stop: '6322 S LAKEVIEW ST',
                            pickup_time: '12:29 PM',
                            dropoff_time: '4:00 PM'
                        }
                    ]
    
                    expect(subject.data[:riders]).to eq expected_riders_object
                end
            end      
        end
    end

    describe '#import_riders' do
        let(:riders) do
            [
                {
                    first_name: 'Barry',
                    last_name: 'Bailey',
                    pickup_route: 'L08 MID2',
                    dropoff_route: 'L58 PM 4',
                    pickup_stop: '3631 E Mineral Pl',
                    dropoff_stop: '3631 E Mineral Pl',
                    pickup_time: '12:26 PM',
                    dropoff_time: '3:45 PM'
                }
            ]
        end

        let(:data) do 
            { riders: riders, routes: {} }
        end

        before { allow_any_instance_of(CSVImporter).to receive(:load).and_return(data) }
        
        subject { CSVImporter.new('') }

        context 'when there is one rider' do
            before { subject.import_riders }
            let(:rider) { Rider.first }
            let(:first_stop) { Stop.first }

            it 'creates the Rider' do
                expect(Rider.count).to eq 1
            end

            it 'creates the stops and routes' do
                expect(Stop.count).to eq 2
                expect(Route.count).to eq 2
            end

            it 'creates the relations between rider and routes' do
                expect(rider.routes.count).to eq 2
            end

            it 'creates the relations between rider and stops' do
                expect(rider.stops.count).to eq 2
            end
        end
    end

    describe '#import_routes_and_stops' do
        let(:routes) do
            {
                'L08 MID2' => { '3631 E Mineral Pl' => ['12:26 PM'] }
            }
        end

        let(:data) do 
            { riders: {}, routes: routes }
        end

        before { allow_any_instance_of(CSVImporter).to receive(:load).and_return(data) }

        subject { CSVImporter.new('') }

        context 'when there is a single route and stop' do
            before { subject.import_routes_and_stops }
            let(:route) { Route.first }
            let(:stop) { Stop.first }

            it 'creates the Stop' do
                expect(Stop.count).to eq 1
            end

            it 'creates the Route' do
                expect(Route.count).to eq 1
            end

            it 'creates the relations between Route and Stop' do
                expect(route.stops.count).to eq 1
            end

            it 'creates the relations between Stop and Route' do
                expect(stop.routes.count).to eq 1
            end
        end
    end
end