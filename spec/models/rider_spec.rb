require 'spec_helper'
require 'models/rider'

RSpec.describe Rider, type: :model do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }

    it { should have_many(:riders_stops) }
    it { should have_many(:stops).through(:riders_stops) }

    it { should have_many(:riders_routes) }
    it { should have_many(:routes).through(:riders_routes) }
end