require 'spec_helper'
require 'models/riders_route'

RSpec.describe RidersRoute, type: :model do
    it { should validate_presence_of(:rider_id) }
    it { should validate_presence_of(:route_id) }

    it { should belong_to(:rider) }
    it { should belong_to(:route) }
end