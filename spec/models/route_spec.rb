require 'spec_helper'
require 'models/route'

RSpec.describe Route, type: :model do
    subject { Route.new(name: 'L08') }

    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }

    it { should have_many(:riders_routes) }
    it { should have_many(:riders).through(:riders_routes) }

    it { should have_many(:routes_stops) }
    it { should have_many(:stops).through(:routes_stops) }
end