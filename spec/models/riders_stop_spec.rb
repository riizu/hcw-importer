require 'spec_helper'
require 'models/riders_stop'

RSpec.describe RidersStop, type: :model do
    it { should validate_presence_of(:rider_id) }
    it { should validate_presence_of(:stop_id) }

    it { should belong_to(:rider) }
    it { should belong_to(:stop) }
end