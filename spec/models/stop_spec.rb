require 'spec_helper'
require 'models/stop'

RSpec.describe Stop, type: :model do
    it { should validate_presence_of(:time) }
    it { should validate_presence_of(:address) }

    it { should have_many(:riders_stops) }
    it { should have_many(:riders).through(:riders_stops) }

    it { should have_many(:routes_stops) }
    it { should have_many(:routes).through(:routes_stops) }
end