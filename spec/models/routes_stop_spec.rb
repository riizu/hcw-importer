require 'spec_helper'
require 'models/routes_stop'

RSpec.describe RoutesStop, type: :model do
    it { should validate_presence_of(:route_id) }
    it { should validate_presence_of(:stop_id) }

    it { should belong_to(:route) }
    it { should belong_to(:stop) }
end