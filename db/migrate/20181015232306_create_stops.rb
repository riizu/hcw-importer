class CreateStops < ActiveRecord::Migration[5.2]
  def change
    create_table :stops do |t|
      t.time :time, null: false
      t.string :address, null: false
    end
  end
end
