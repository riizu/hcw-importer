class CreateRidersRoutes < ActiveRecord::Migration[5.2]
  def change
    create_join_table :riders, :routes do |t|
      t.index :rider_id
      t.index :route_id
    end
  end
end
