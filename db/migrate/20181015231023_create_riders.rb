class CreateRiders < ActiveRecord::Migration[5.2]
  def change
    create_table :riders do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
    end
  end
end
