class CreateRidersStops < ActiveRecord::Migration[5.2]
  def change
    create_join_table :riders, :stops do |t|
      t.index :rider_id
      t.index :stop_id
    end
  end
end
