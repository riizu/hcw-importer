# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_16_155816) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "riders", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
  end

  create_table "riders_routes", id: false, force: :cascade do |t|
    t.bigint "rider_id", null: false
    t.bigint "route_id", null: false
    t.index ["rider_id"], name: "index_riders_routes_on_rider_id"
    t.index ["route_id"], name: "index_riders_routes_on_route_id"
  end

  create_table "riders_stops", id: false, force: :cascade do |t|
    t.bigint "rider_id", null: false
    t.bigint "stop_id", null: false
    t.index ["rider_id"], name: "index_riders_stops_on_rider_id"
    t.index ["stop_id"], name: "index_riders_stops_on_stop_id"
  end

  create_table "routes", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "routes_stops", id: false, force: :cascade do |t|
    t.bigint "route_id", null: false
    t.bigint "stop_id", null: false
    t.index ["route_id"], name: "index_routes_stops_on_route_id"
    t.index ["stop_id"], name: "index_routes_stops_on_stop_id"
  end

  create_table "stops", force: :cascade do |t|
    t.time "time", null: false
    t.string "address", null: false
  end

end
